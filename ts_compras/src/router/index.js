import { createRouter, createWebHistory } from 'vue-router'
import App from '../App.vue'

import ConfirmarPago from '../components/ConfirmarPago.vue'
import ConfirmarCompra from '../components/ConfirmarCompra.vue'

const routes = [{
 path: '/',
 name: 'app',
 component: App
},
{
    path: '/compra',
    name: "ConfirmarCompra",
    component: ConfirmarCompra
},
{
    path: '/compra/pago',
    name: "ConfirmarPago",
    component: ConfirmarPago
    
},
]
const router = createRouter({
 history: createWebHistory(),
 routes
})
export default router